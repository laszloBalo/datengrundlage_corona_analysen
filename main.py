"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
import datetime
from time import sleep
from scripts.helper_functions.read_write import *
from scripts.helper_functions.web_crawler_setup import * 

if __name__ == "__main__":
    print("########        Datengrundlage_Corona_Analysen        ########\n") 

    # check for folder struct 
    path_root = os.path.abspath(".")
    config_file = check_config(path_root)
    check_folder_struct(path_root,config_file)    

    # setup all web_crawlers defined in the config files
    list_web_crawler = web_crawler_setup(path_root,config_file)

    """ crawl data in the past until now"""
    for web_crawler in list_web_crawler:
        try:
            print(f"\nNote: {web_crawler.name} start (past)")
            web_crawler.get_data("past")
            print(f"Note: {web_crawler.name} successful (past)\n")
        except Exception as error:
            print(f"Error: crawler past -> {web_crawler.name}")
            print(str(error) + "\n")

    """ set up update cycle """    
    update_cycle_h      = config_file['settings']['update_cycle_h']
    update_amount_total = config_file['settings']['update_amount']
    update_amount       = update_amount_total
    update_current      = 1 

    while update_amount > 0:
        
        print(f"Note: Update {update_current}/{update_amount_total}")

        """ crawl data updates """
        for web_crawler in list_web_crawler:
            try:
                print(f"\nNote: {web_crawler.name} start")
                web_crawler.get_data("today")
                print(f"Note: {web_crawler.name} successful\n")
            except Exception as error:
                print(f"Error: crawler today -> {web_crawler.name}")
                print(str(error) + "\n")
        
        print("# Finished past crawling ")

        """ for every crawler collect data in the dev folder """
        for web_crawler in list_web_crawler:
            try:
                print(f"\nNote: {web_crawler.name} start process data")
                web_crawler = web_crawler.gather_dev_files()
                web_crawler.process_dev_files()
                print(f"Note: {web_crawler.name} successfully process data\n")
            except Exception as error:
                print(f"Error: crawler when processing data -> {web_crawler.name}!")
                print(str(error) + "\n")

        """ wait """
        current_time = datetime.datetime.now()
        current_time_str = current_time.strftime("%d-%m-%Y %H:%M:%S")

        next_update  = current_time + datetime.timedelta(hours=update_cycle_h)
        next_update_str = next_update.strftime("%d-%m-%Y %H:%M:%S")

        if update_current != update_amount_total:
            print(f"Note: Update {update_current}/{update_amount_total}")
            print(f"Note: Wait for {update_cycle_h} h")
            print(f"\t- From: {current_time_str}")
            print(f"\t- To: {next_update_str}")
            sleep(update_cycle_h*3600) 

             
        else:
            print(f"Note: Update {update_current}/{update_amount_total}")
        

        # reduce loop vars 
        update_amount   -= 1
        update_current  += 1 
            
    print("\n########        Datengrundlage_Corona_Analysen        ########")
    