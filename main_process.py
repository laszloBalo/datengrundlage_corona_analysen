"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 

from scripts.helper_functions.read_write import *
from scripts.helper_functions.web_crawler_setup import web_crawler_setup

if __name__ == "__main__":
    print("########        Datengrundlage_Corona_Analysen        ########\n") 
    # check for folder struct 
    path_root = os.path.abspath(".")
    config_file = check_config(path_root)
    check_folder_struct(path_root,config_file)  

    # setup all web_crawlers defined in the config files
    list_web_crawler = web_crawler_setup(path_root,config_file)

    """ for every crawler collect data in the dev folder """
    for web_crawler in list_web_crawler:
        try:
            print(f"\nNote: {web_crawler.name} start process data")
            web_crawler = web_crawler.gather_dev_files()
            web_crawler.process_dev_files()
            print(f"Note: {web_crawler.name} successfully process data\n")
        except Exception as error:
            print(f"Error: crawler when processing data -> {web_crawler.name}!")
            print(str(error) + "\n")

    print("\n########        Datengrundlage_Corona_Analysen        ########")

    
