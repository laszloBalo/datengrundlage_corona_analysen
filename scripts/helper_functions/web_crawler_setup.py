"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

# imports 
import os 
from time import gmtime, strftime, localtime

from scripts.module.web_crawler.crawler_ECDC                        import crawler_ECDC
from scripts.module.web_crawler.crawler_JHU_API                     import crawler_JHU_API
from scripts.module.web_crawler.crawler_WHO_dashboard               import crawler_WHO_dashboard
from scripts.module.web_crawler.crawler_AUT_datenfakten             import crawler_AUT_datenfakten
from scripts.module.web_crawler.crawler_AUT_gesundheitsministerium  import crawler_AUT_gesundheitsministerium
from scripts.module.web_crawler.crawler_ECDC_API                    import crawler_ECDC_API

def current_time():
    """
    Get current date in specific format 
    input: None 
    output: time in format: year_month_day__hour_sec as string 
    """

    time_str = strftime("%Y_%m_%d__%H_%M_%S", localtime())

    return time_str

def web_crawler_setup(path_root,config_file):
    """
    input: 
        - path_root -> String 
        - config_file -> dictionary 
    output: 
        - web_crawler -> List -> web_crwaler objects 
    
    get the given web crawler or api object for the task 
    """

    # list with web_crawlers 
    list_web_crawler = []

    # go through all levels and init crawlers that have an class defined 
    for level, sub_dict in config_file["web"].items():
        # go through trust types 
        for trust, crawler_names in sub_dict.items():
            # go through all crawler names 
            for crawler_name in crawler_names:
                if crawler_name == "ECDC_API":
                    list_web_crawler.append(
                        crawler_ECDC_API(
                            current_time(),
                            crawler_name,
                            trust,
                            config_file,
                            "https://api.statworx.com/covid",
                            level
                            )
                    )
                elif crawler_name == "JHU_API":
                    list_web_crawler.append(
                        crawler_JHU_API(
                            current_time(),
                            crawler_name,
                            trust,
                            config_file,
                            "https://covidapi.info",
                            level
                            )
                    )
                elif crawler_name == "AUT_gesundheitsministerium":
                    list_web_crawler.append(
                        crawler_AUT_gesundheitsministerium(
                            current_time(),
                            crawler_name,
                            trust,
                            config_file,
                            "https://info.gesundheitsministerium.at",
                            level
                            )
                    )
                elif crawler_name == "AUT_datenfakten":
                    list_web_crawler.append(
                        crawler_AUT_datenfakten(
                            current_time(),
                            crawler_name,
                            trust,
                            config_file,
                            "https://coronavirus.datenfakten.at/",
                            level
                            )
                    )
                elif crawler_name == "WHO_dashboard":
                    list_web_crawler.append(
                        crawler_WHO_dashboard(
                            current_time(),
                            crawler_name,
                            trust,
                            config_file,
                            "https://who.sprinklr.com/region/euro/country/de",
                            level
                            )
                    )
                elif crawler_name == "ECDC":
                    list_web_crawler.append(
                        crawler_ECDC(
                            current_time(),
                            crawler_name,
                            trust,
                            config_file,
                            "https://opendata.ecdc.europa.eu/covid19/casedistribution/csv",
                            level
                            )
                    )
                else:
                    print(f"Warning: Can't match crawler class for \"{crawler_name}\"")



    return list_web_crawler 