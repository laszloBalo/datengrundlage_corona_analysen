"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
import sys 
import json

def check_config(path_root):
    """
    input: path_root -> String 
    output: config file as dictionary 
    read config file if possible 
    """

    try:
        path_json = os.path.join(path_root, "config", "config.json") 
        dict_config = read_json_2_dict(path_json)

        # add path_root 
        dict_config["path_root"] = path_root


    except:
        print("Error: could not find config file! add config file in the config folder ... ")
        sys.exit()


    return dict_config

def check_folder_struct(path_root, config_file):
    """
    input:  root_path -> String
    output: check     -> Bool
    check folder struct that is defined in the config json 
    """

    # go through all folders 
    for folder, sub_folders in config_file["folders"].items():
        path_folder = os.path.join(path_root,folder)
        bool_exist = os.path.exists(path_folder)

        if not bool_exist:
            os.mkdir(path_folder)
            print(f"Warning: created folder -> {path_folder}")

        for sub_folder in sub_folders:
            path_sub_folder = os.path.join(path_root,folder,sub_folder)
            bool_exist = os.path.exists(path_sub_folder)

            if not bool_exist:
                os.mkdir(path_sub_folder)
                print(f"Warning: created folder -> {path_sub_folder}")

def write_dict_2_json(path_file,dict_data):
    """
    input: 
        - path_file -> String 
        - dict_data -> dictionary 
    output:
        - None  

    Write dictionary to json file
    """

    with open(path_file, 'w+') as file:
        json.dump(dict_data, file, indent=4, sort_keys=True)

    print(f"Note: wrote \"{path_file}\"")

def read_json_2_dict(path_file):
    """
    input: path_file -> String
    output: dict_data -> dictionary 

    Read json to dict
    """
    try:
        with open(path_file,"r") as file:
            dict_data = json.load(file)
        
        print(f"Note: read successfully \"{path_file}\"")

    except OSError:
        print(f"Error:\t could not read -> {path_file}")
        dict_data = None
        
    finally:
        return dict_data

def del_processed_files(list_files):
    """
    input: list_files -> list
    delete all files that where processed by a given crawler
    """

    if len(list_files) > 0:
        print(f"Note: Deleted already processed files: ")
        for path_file in list_files:
            os.remove(path_file)
            print(f" - {path_file}")

def move_to_backup(crawler, path_files):
    """
    input: path_files -> list 
    move dev files to backup folder (regarding crawler)
    """

    # check if given backup folder exists 
    path_folder_backup = os.path.join(crawler.config['path_root'], "data", "backup", crawler.name)

    if not os.path.exists(path_folder_backup): 
        os.mkdir(path_folder_backup)
        print(f"Note: Backup folder \"{crawler.name}\" was created")

    for path_file in path_files:
        path_file_backup = os.path.join(path_folder_backup, path_file.split("/")[-1])

        os.replace(path_file,path_file_backup)
        print(f"Note: moved file to \"{path_file_backup}\"")

def move_to_corrupt(crawler, path_files):
    """
    input: path_files -> list 
    move dev files to corrupt folder (regarding crawler)
    """

    # check if given backup folder exists 
    path_folder_backup = os.path.join(crawler.config['path_root'], "data", "corrupt", crawler.name)

    if not os.path.exists(path_folder_backup): 
        os.mkdir(path_folder_backup)
        print(f"Note: Backup folder \"{crawler.name}\" was created")

    for path_file in path_files:
        path_file_backup = os.path.join(path_folder_backup, path_file.split("/")[-1])

        os.replace(path_file,path_file_backup)
        print(f"Note: moved file to \"{path_file_backup}\"")


