"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
from time import gmtime, strftime, localtime

class web_crawler():
    
    def __init__(self, date, name, trust, config, web_site, level, data = None, log = None, path_dev = {}):
        self.date               = date 
        self.data               = data 
        self.name               = name 
        self.config             = config
        self.web_site           = web_site
        self.level              = level
        self.log                = log
        self.trust              = trust
        self.path_dev           = path_dev

    def print_simple(self):
        """
        simple print of web_crawler class
        """

        print(f"web_crawler: ")
        print(f"\tname     : {self.name}")
        print(f"\ttrust    : {self.trust}")
        print(f"\tcreated  : {self.date}")
        print(f"\tweb_site : {self.web_site}")

    def current_time(self):
        """
        Get current date in specific format 
        input: None 
        output: time in format: year-month-day
        """

        time_str = strftime("%Y-%m-%d", gmtime())

        return time_str

    def get_path_tmp(self, crawler_range, file_type = ".json"):
        """
        create tmp path for development
        """

        file_name   = self.name + "_" + self.trust +"_" + crawler_range + "_" + self.date + file_type
        path_tmp    = os.path.join(self.config["path_root"], "data" , "dev", file_name)

        return path_tmp

    def gather_dev_files(self):
        """
        gather dev files from dev folder regarding this crawler 
        """
        
        path_folder_dev = os.path.join(self.config['path_root'],"data","dev")
        files = os.listdir(path_folder_dev)

        self.path_dev = {
            "past" : [],
            "today" : [],
            "zip"   : []
        }

        for f_name in files:
            if self.name in f_name and "today" in f_name:
                self.path_dev["today"].append(
                    os.path.join(path_folder_dev, f_name)
                )
            elif self.name in f_name and "past" in f_name:
                self.path_dev["past"].append(
                    os.path.join(path_folder_dev, f_name)
                )
            elif self.name in f_name and "zip" in f_name:
                self.path_dev["zip"].append(
                    os.path.join(path_folder_dev, f_name)
                )

        if len(self.path_dev["today"]) == 0 and len(self.path_dev["past"]) == 0 and len(self.path_dev["zip"]) == 0 :
            print(f"Warning: There are no files for crawler \"{self.name}\" to process")

        return self

    def current_time_machine(self):
        """
        Get current date in specific format 
        input: None 
        output: time in format: year_month_day__hour_sec as string 
        """

        time_str = strftime("%Y_%m_%d__%H_%M_%S", localtime())

        return time_str