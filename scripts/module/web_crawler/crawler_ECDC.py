"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""
import os 
import pandas as pd
import numpy as np
import io
import requests
from datetime import datetime, timezone

from scripts.helper_functions.read_write import write_dict_2_json, del_processed_files
from scripts.module.web_crawler.crawler import web_crawler
from scripts.module.web_driver_chrome import web_driver_chrome

class crawler_ECDC(web_crawler):
    
    def __init__(self, date, name, trust, config, web_site, level, data = None, log = None, path_dev = {}):
        super().__init__(date, name, trust, config, web_site, level, data = None, log = None, path_dev = {})

    def get_data(self,crawler_range):
        """
        input: range -> defines which range we want to crawl
        gather data from ECDC
        """

        if crawler_range == "today": 
            
            url = "https://opendata.ecdc.europa.eu/covid19/casedistribution/csv"
            s   = requests.get(url).content
            df  = pd.read_csv(io.StringIO(s.decode("utf-8-sig")), na_values="", sep=",")

            path_csv = self.get_path_tmp(crawler_range, "csv")
            df.to_csv(path_csv, index=False)
            print(f"Note: wrote \"{path_csv}\"")
            
        elif crawler_range == "past":
            print("past that crawling makes no scense at https://who.sprinklr.com/region/euro/country/de")
            raise Exception

        else:
            print(f"Warning: range is not defined in crawler_JHU_API -> {crawler_range}")

        return self
    
    def process_dev_files(self):
        """
        process data and add it to the collection 
        """

        # files to delte after processing
        files_to_del = [] 
        # files that have to be merged 
        files_to_combine = []
        # check if there exists a level_1 collections csv
        path_level_1 = os.path.join(self.config['path_root'],"data","collect",self.level + ".csv")

        if os.path.exists(path_level_1):
            df_old = pd.read_csv(path_level_1)

            # check if col exists 
            if "cumulative_recovered" not in df_old.columns:
                df_old = df_old.assign(recovered = np.nan)
                df_old = df_old.assign(cumulative_recovered = np.nan) 

            files_to_combine = [df_old]

        for update_type, path_files in self.path_dev.items():
            if update_type == "today":
                for path_file in path_files:
                    if "API" not in path_file: 
                        df = pd.read_csv(path_file)
                        # add crawler info to rows 
                        # cols = crawler_name,trust,day,country,region,deaths,cumulative_deaths,confirmed,cumulative_confirmed
                        df = df.assign(day = lambda  df: df["dateRep"].map(lambda x: datetime(int(x.split("/")[2]), int(x.split("/")[1]), int(x.split("/")[0]), tzinfo=timezone.utc).timestamp()*1000))

                        df = df.rename(columns={
                            "geoId" : "country",
                            "cases" : "confirmed",
                            })

                        df = df.assign(crawler_name = self.name)
                        df = df.assign(trust = self.trust)
                        df = df.assign(cumulative_deaths = "")
                        df = df.assign(cumulative_confirmed = "")
                        df = df.assign(region = "")
                        df = df.assign(recovered = np.nan)
                        df = df.assign(cumulative_recovered = np.nan) 

                        cols_to_drop = ['dateRep', 'month', 'year',
                                        'countriesAndTerritories', 'countryterritoryCode',
                                        'popData2018']
                        df = df.drop(cols_to_drop, axis=1)

                        files_to_combine.append(df)
                        files_to_del.append(path_file) 

                # combine files 
                combined = pd.concat(files_to_combine).sort_values(["day","confirmed","deaths"],ascending=[False,False,False]).reset_index(drop=True)
                combined = combined.drop_duplicates(["crawler_name","day","country"])
                    
                combined.to_csv(path_level_1, index = False)

                del_processed_files(files_to_del)

                print(f"Note: Update -> \"{path_level_1}\"")

            elif update_type == "past" and len(path_files) > 0:
                print(f"Warning: past processing not implemented yet in {self.name}")