"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
import requests
import re
import pandas as pd 
import numpy as np

from scripts.helper_functions.read_write import write_dict_2_json, read_json_2_dict, del_processed_files
from scripts.module.web_crawler.crawler import web_crawler
from scripts.module.web_driver_chrome import web_driver_chrome

class crawler_WHO_dashboard(web_crawler):
    
    def __init__(self, date, name, trust, config, web_site, level, data = None, log = None, path_dev = {}):
        super().__init__(date, name, trust, config, web_site, level, data = None, log = None, path_dev = {})

    def get_data(self,crawler_range):
        """
        input: range -> defines which range we want to crawl
        gather data from WHO dashboard
        """

        if crawler_range == "today": 
            
            path_web_driver = os.path.join(self.config["path_root"],"browser","chrome","chromedriver")
            
            dict_json = requests.get("https://dashboards-dev.sprinklr.com/data/9043/global-covid19-who-gis.json").json()
            self.data = dict_json
            write_dict_2_json(self.get_path_tmp(crawler_range),self.data) 
            

        elif crawler_range == "past":
            print("past that crawling makes no scense at https://who.sprinklr.com/region/euro/country/de")
            raise Exception

        else:
            print(f"Warning: range is not defined in crawler_JHU_API -> {crawler_range}")

        return self

    def process_dev_files(self):
        """
        process data and add it to the collection 
        """

        # files to delte after processing
        files_to_del = [] 
        # files that have to be merged 
        files_to_combine = []
        # check if there exists a level_1 collections csv
        path_level_1 = os.path.join(self.config['path_root'],"data","collect",self.level + ".csv")

        if os.path.exists(path_level_1):
            df_old = pd.read_csv(path_level_1)

            # check if col exists 
            if "cumulative_recovered" not in df_old.columns:
                df_old = df_old.assign(recovered = np.nan)
                df_old = df_old.assign(cumulative_recovered = np.nan) 

            files_to_combine = [df_old]

        for update_type, path_files in self.path_dev.items():
            if update_type == "today":
                for path_file in path_files:
                    dict_file = read_json_2_dict(path_file)
                    # add crawler info to rows and at the end fillers for recovered people
                    rows = [[self.name,self.trust] + row + [np.nan, np.nan] for row in dict_file['rows']]

                    df = pd.DataFrame(rows,columns=self.config['column']['level_1'])    
                    files_to_combine.append(df)
                    files_to_del.append(path_file) 

                # combine files 
                combined = pd.concat(files_to_combine).sort_values(["day","confirmed","deaths"],ascending=[False,False,False]).reset_index(drop=True)
                combined = combined.drop_duplicates(["crawler_name","day","country"])
                    
                combined.to_csv(path_level_1, index = False)

                del_processed_files(files_to_del)

                print(f"Note: Update -> \"{path_level_1}\"")

            elif update_type == "past" and len(path_files) > 0:
                print(f"Warning: past processing not implemented yet in {self.name}")
                



            

