"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
import requests
import pandas as pd 
import numpy as np
import json 
import pytz 
import zipfile
from datetime import datetime, timezone

from scripts.helper_functions.read_write import write_dict_2_json, read_json_2_dict, del_processed_files, move_to_backup
from scripts.module.web_crawler.crawler import web_crawler
from scripts.module.web_driver_chrome import web_driver_chrome

class crawler_ECDC_API(web_crawler):
    
    def __init__(self, date, name, trust, config, web_site, level, data = None, log = None, path_dev = {}):
        super().__init__(date, name, trust, config, web_site, level, data = None, log = None, path_dev = {})

    def get_data(self,crawler_range):
        """
        input: range -> defines which range we want to crawl
        gather data from gesundheitsministerium dashboard 
        """

        def get_europe_data(list_country):

            list_df = []
            URL = 'https://api.statworx.com/covid'

            for country in list_country:
                payload = {'code': country}
                response = requests.post(url=URL, data=json.dumps(payload))
                # Convert to data frame
                list_df.append(pd.DataFrame.from_dict(json.loads(response.text)))

            df_eu = pd.concat(list_df)
            return df_eu


        if crawler_range == "today": 
            
            #list_country = ["BE","BG","CZ","DK","DE","EE","IE","EL","ES","FR","HR","IT","CY","LV","LT","LU","HU","MT","NL","AT","PL","PT","RO","SI","SK","FI","SE"]
            list_country = ["ALL"]
            df_eu = get_europe_data(list_country)
            df_eu.to_csv(self.get_path_tmp(crawler_range,file_type=".csv"),index=False)

        elif crawler_range == "past":
            print("past that crawling makes no scense, ECDC_API")
            raise Exception

        else:
            print(f"Error: range is not defined in crawler_ECDC -> {crawler_range}")

        return self

    def process_dev_files(self): 
        """
        process data and add it to the collection 
        """

        # files to delte after processing
        files_to_del = [] 
        # files that have to be merged 
        files_to_combine = []
        # check if there exists a level_1 collections csv
        path_level_2 = os.path.join(self.config['path_root'],"data","collect",self.level + ".csv")

        if os.path.exists(path_level_2):
            files_to_combine = [pd.read_csv(path_level_2)]

        for update_type, path_files in self.path_dev.items():
            if update_type == "today":
                for path_file in path_files:

                    df = pd.read_csv(path_file)

                    df = df[df["continentExp"] == "Europe"]
                    df = df.assign(date = lambda  df: df["date"].map(lambda x: datetime(int(x.split("-")[0]), int(x.split("-")[1]), int(x.split("-")[2]), tzinfo=timezone.utc).timestamp()*1000))
                    df = df.assign(crawler_name = self.name)
                    df = df.assign(trust = self.trust)

                    cols_to_drop = ['day', 'month', 'year']
                    df = df.drop(cols_to_drop, axis=1)

                    files_to_combine.append(df)
                    files_to_del.append(path_file) 

                # combine files 
                combined = pd.concat(files_to_combine).sort_values(['date', 'crawler_name'],ascending=[False,False]).reset_index(drop=True)
                combined = combined.drop_duplicates(['crawler_name','date',"code"])
                combined.to_csv(path_level_2, index = False)

                # make a backup and del dev files 
                move_to_backup(self, files_to_del)

            elif update_type == "past" and len(path_files) > 0:
                print(f"Warning: past processing not implemented yet in {self.name}")
    