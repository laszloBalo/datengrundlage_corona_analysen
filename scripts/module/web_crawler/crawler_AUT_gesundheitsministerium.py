"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
import requests
import re
import pandas as pd 
import numpy as np
import pytz 
import datetime
import zipfile

from time import sleep

from scripts.helper_functions.read_write import write_dict_2_json, read_json_2_dict, del_processed_files, move_to_backup, move_to_corrupt
from scripts.module.web_crawler.crawler import web_crawler
from scripts.module.web_driver_chrome import web_driver_chrome

class crawler_AUT_gesundheitsministerium(web_crawler):
    
    def __init__(self, date, name, trust, config, web_site, level, data = None, log = None, path_dev = {}):
        super().__init__(date, name, trust, config, web_site, level, data = None, log = None, path_dev = {})

    def get_data(self,crawler_range):
        """
        input: range -> defines which range we want to crawl
        gather data from gesundheitsministerium dashboard 
        """

        def gather_hospitalization(chrome, web_hospitalization):
            
            chrome.driver.get(web_hospitalization)

            table_raw = [i.text for i in chrome.driver.find_elements_by_css_selector("td , th")]

            sub_lists = []
            col_size = 11 
            for row_id in range(0,int(len(table_raw)/col_size)):
                index_start = row_id * 11
                index_end   = row_id * 11 + 11 
                sub_lists.append(table_raw[index_start:index_end])

            df_hospitalization = pd.DataFrame(sub_lists[1:],columns=sub_lists[0])

            return df_hospitalization

        if crawler_range == "today": 
            # setup browser
            path_web_driver = os.path.join(self.config["path_root"],"browser","chrome","chromedriver")
            chrome          = web_driver_chrome(path_web_driver,headless=False)
            chrome          = chrome.init_browser()
            chrome.driver.get(self.web_site)

            # download zip file and rename it 
            chrome.click_button("#csv-btn .nav-link")
            # wait until file is realy downloaded
            sleep(10)
            path_folder = os.path.join(self.config["path_root"],"data","dev")

            for file in [os.path.join(path_folder,f) for f in os.listdir(path_folder) if "data.zip" in f ]:
                new_file = self.name + "_" + self.current_time_machine() + ".zip"
                path_new_zip = os.path.join("/".join(file.split("/")[:-1]),new_file)
                os.rename(file,path_new_zip )
                print(f"Note: crawled zip {new_file}")

            web_hospitalization = "https://www.sozialministerium.at/Informationen-zum-Coronavirus/Neuartiges-Coronavirus-(2019-nCov).html"
            df_hospitalization = gather_hospitalization(chrome,web_hospitalization)
            df_path = os.path.join(path_folder, "hospitalization.csv")
            df_hospitalization.to_csv(df_path, index=False, sep=";")

            with zipfile.ZipFile(path_new_zip, 'a') as zipf:
                source_path = df_path
                destination = "df_hospitalization.csv"
                zipf.write(source_path, destination)

            os.remove(df_path) 

            chrome.driver.close()

        elif crawler_range == "past":
            print("past that crawling makes no scense at https://info.gesundheitsministerium.at")
            raise Exception

        else:
            print(f"Error: range is not defined in crawler_AUT_gesundheitsministerium -> {crawler_range}")

        return self

    def process_dev_files(self): 
        """
        process data and add it to the collection 
        """

        def convert_abbreviation(abb):
            abb = abb.replace(".","")
            if abb == "V":
                state_name = "Vorarlberg"
            elif abb == "Vbg":
                state_name = "Vorarlberg"
            elif abb == "W":
                state_name = "Wien"
            elif abb == "T":
                state_name = "Tirol"
            elif abb == "Stmk":
                state_name = "Steiermark"
            elif abb == "Sbg":
                state_name = "Salzburg"
            elif abb == "OÖ":
                state_name = "Oberösterreich"
            elif abb == "NÖ":
                state_name = "Niederösterreich"
            elif abb == "Ktn":
                state_name = "Kärnten"
            elif abb == "Kt":
                state_name = "Kärnten"
            elif abb == "Bgld":
                state_name = "Burgenland"
            else:
                print(f"Error: cant match state abb to full name -> {abb}")

            return state_name 

        def get_current_data(df_AllgemeinDaten,df_Altersverteilung,df_AltersverteilungTodesfaelle,df_Bezirke,df_Bundesland,df_GenesenTodesFaelleBL,
                            df_Geschlechtsverteilung,df_IBAuslastung,df_IBKapazitaeten,df_NBAuslastung,df_NBKapazitaeten,df_VerstorbenGeschlechtsverteilung,df_hospitalization,
                            self):
            """
            only process data that represents cov-19 regarding current date 
            """

            def process_age(df_Altersverteilung,df_AltersverteilungTodesfaelle):

                for index, row in df_Altersverteilung.iterrows():
                    col_name = "age_cases_" + row["Altersgruppe"]
                    value = row["Anzahl"]

                    dict_current[col_name] = [value]
            
                for index, row in df_AltersverteilungTodesfaelle.iterrows():
                    col_name = "age_deaths_" + row["Altersgruppe"]
                    value = row["Anzahl"]

                    dict_current[col_name] = [value]
            

                return dict_current

            def process_district(df_Bezirke):

                for index, row in df_Bezirke.iterrows():
                    col_name = "district_cases_" + row["Bezirk"].replace(" ","_")
                    value = row["Anzahl"]

                    dict_current[col_name] = [value]

                return dict_current

            def process_state(df_Bundesland,df_GenesenTodesFaelleBL):
                
                for index, row in df_Bundesland.iterrows():
                    
                    state_name = row["Bundesland"]

                    col_name = "state_ill_" + state_name
                    value = row["Anzahl"]

                    dict_current[col_name] = [value]
                
                for index, row in df_GenesenTodesFaelleBL.iterrows():

                    col_recovered   = "state_recovered_" + row["Bundesland"]
                    value_recovered = row["Genesen"]

                    col_deaths   = "state_deaths_" + row["Bundesland"]
                    value_deaths = row["Todesfälle"]

                    dict_current[col_recovered] = [value_recovered]
                    dict_current[col_deaths]    = [value_deaths]

                return dict_current

            def process_general(df_AllgemeinDaten, df_Geschlechtsverteilung, df_VerstorbenGeschlechtsverteilung):

                dict_current["tests_sum"] = int(df_AllgemeinDaten["GesTestungen"])
                dict_current["tests_sum_pos"] = int(df_AllgemeinDaten["PositivGetestet"])

                for index, row in df_Geschlechtsverteilung.iterrows():
                        col_name = "gender_dist_" + row["Geschlecht"]
                        value = row["Anzahl in %"]

                        dict_current[col_name] = [value] 

                for index, row in df_VerstorbenGeschlechtsverteilung.iterrows():
                        col_name = "gender_dist_deaths_" + row["Geschlecht"]
                        value = row["Anzahl in %"]

                        dict_current[col_name] = [value] 

                return dict_current

            def process_hospital(df_hospitalization,df_IBAuslastung,df_IBKapazitaeten,df_NBAuslastung,df_NBKapazitaeten):
                
                def repair_table_value(value):
                    re_float_err = re.compile(r"(.+?)0{3,}")
                    try: 
                        new_value = int(re.findall(re_float_err,str(value).replace(".",""))[0])
                    except: 
                        new_value = value 

                    return new_value 

                for index, row in df_hospitalization.iterrows():
                    if "Bestätigte Fälle" in row["Bundesland"]:
                        pass
                    elif "Todesfälle" in row["Bundesland"]:
                        pass
                    elif "Genesen" in row["Bundesland"]:
                        pass
                    elif "Hospitalisierung" in row["Bundesland"]:
                        for ivalue, value in enumerate(row.values[1:-1],1):
                            state_name = convert_abbreviation(df_hospitalization.columns[ivalue])
                            col_name = "hospitalization_" + state_name
                            dict_current[col_name] = [repair_table_value(value)] 
                    elif "Intensivstation" in row["Bundesland"]:
                        for ivalue, value in enumerate(row.values[1:-1],1):
                            state_name = convert_abbreviation(df_hospitalization.columns[ivalue])
                            col_name = "intensive_" + state_name
                            dict_current[col_name] = [repair_table_value(value)] 
                    elif "Testungen" in row["Bundesland"]:
                        for ivalue, value in enumerate(row.values[1:-1],1):
                            state_name = convert_abbreviation(df_hospitalization.columns[ivalue])
                            col_name = "tests_" + state_name
                            dict_current[col_name] = [repair_table_value(value)] 

                # capacity/utilization intensive 
                intensive_utilization = df_IBAuslastung.iloc[-1]
                dict_current["intensive_utilization"] = [intensive_utilization["Belegung Intensivbetten in %"]] 
                intensive_capacity = df_IBKapazitaeten.iloc[-1]
                dict_current["intensive_capacity"] = [intensive_capacity["Kapazität Intensivbetten in %"]]

                # normal bed 
                hospitalization_utilization = df_NBAuslastung.iloc[-1]
                dict_current["hospitalization_utilization"] = [hospitalization_utilization["Belegung Normalbetten in %"]] 
                hospitalization_capacity = df_NBKapazitaeten.iloc[-1]
                dict_current["hospitalization_capacity"] = [hospitalization_capacity["Kapazität Normalbetten in %"]]

                return dict_current 

            dict_current = {col : [np.nan] for col in self.config['column']['level_3']}

            dict_current["crawler_name"] = [self.name]
            dict_current["trust"]        = [self.trust]
            
            # utc time 
            datetime_raw = df_AllgemeinDaten["LetzteAktualisierung"].values[0]
            local = pytz.timezone("Europe/Vienna")
            naive = datetime.datetime.strptime(datetime_raw, "%d.%m.%Y %H:%M:%S")
            local_dt = local.localize(naive, is_dst=None)
            utc_dt = local_dt.astimezone(pytz.utc).timestamp() * 1000
            dict_current["datetime"] = [utc_dt]

            dict_current = process_age(df_Altersverteilung,df_AltersverteilungTodesfaelle)
            dict_current = process_district(df_Bezirke)
            dict_current = process_state(df_Bundesland,df_GenesenTodesFaelleBL)
            dict_current = process_general(df_AllgemeinDaten, df_Geschlechtsverteilung, df_VerstorbenGeschlechtsverteilung)
            dict_current = process_hospital(df_hospitalization,df_IBAuslastung,df_IBKapazitaeten,df_NBAuslastung,df_NBKapazitaeten)

            return dict_current 

        # files to delte after processing
        files_to_del = [] 
        # files that have to be merged 
        files_to_combine = []
        # check if there exists a level_1 collections csv
        path_level_3 = os.path.join(self.config['path_root'],"data","collect",self.level + ".csv")

        if os.path.exists(path_level_3):
            files_to_combine = [pd.read_csv(path_level_3)]

        for update_type, path_files in self.path_dev.items():
            if update_type == "zip":
                for path_file in path_files:
                    try:
                        with zipfile.ZipFile(path_file, 'r') as zip_folder:
                            # load all files that are in zip 
                            df_AllgemeinDaten = pd.read_csv(zip_folder.open("AllgemeinDaten.csv"),sep=";")
                            df_Altersverteilung = pd.read_csv(zip_folder.open("Altersverteilung.csv"),sep=";")
                            df_AltersverteilungTodesfaelle = pd.read_csv(zip_folder.open("AltersverteilungTodesfaelle.csv"),sep=";")
                            df_AltersverteilungTodesfaelleDemogr = pd.read_csv(zip_folder.open("AltersverteilungTodesfaelleDemogr.csv"),sep=";")
                            df_Bezirke = pd.read_csv(zip_folder.open("Bezirke.csv"),sep=";")
                            df_Bundesland = pd.read_csv(zip_folder.open("Bundesland.csv"),sep=";")
                            df_Epikurve = pd.read_csv(zip_folder.open("Epikurve.csv"),sep=";")
                            df_GenesenTimeline = pd.read_csv(zip_folder.open("GenesenTimeline.csv"),sep=";")
                            df_GenesenTodesFaelleBL = pd.read_csv(zip_folder.open("GenesenTodesFaelleBL.csv"),sep=";")
                            df_Geschlechtsverteilung = pd.read_csv(zip_folder.open("Geschlechtsverteilung.csv"),sep=";")
                            df_IBAuslastung = pd.read_csv(zip_folder.open("IBAuslastung.csv"),sep=";")
                            df_IBKapazitaeten = pd.read_csv(zip_folder.open("IBKapazitaeten.csv"),sep=";")
                            df_NBAuslastung = pd.read_csv(zip_folder.open("NBAuslastung.csv"),sep=";")
                            df_NBKapazitaeten = pd.read_csv(zip_folder.open("NBKapazitaeten.csv"),sep=";")
                            df_TodesfaelleTimeline = pd.read_csv(zip_folder.open("TodesfaelleTimeline.csv"),sep=";")
                            df_VerstorbenGeschlechtsverteilung = pd.read_csv(zip_folder.open("VerstorbenGeschlechtsverteilung.csv"),sep=";")
                            df_hospitalization = pd.read_csv(zip_folder.open("df_hospitalization.csv"),sep=";")

                        dict_current = get_current_data(
                            df_AllgemeinDaten,
                            df_Altersverteilung,
                            df_AltersverteilungTodesfaelle,
                            df_Bezirke,
                            df_Bundesland,
                            df_GenesenTodesFaelleBL,
                            df_Geschlechtsverteilung,
                            df_IBAuslastung,
                            df_IBKapazitaeten,
                            df_NBAuslastung,
                            df_NBKapazitaeten,
                            df_VerstorbenGeschlechtsverteilung,
                            df_hospitalization,
                            self)

                        df = pd.DataFrame.from_dict(dict_current)
                        files_to_combine.append(df)
                        files_to_del.append(path_file) 
                    except:
                        print(f"Error: corrupt zip file {path_file}")
                        move_to_corrupt(self, [path_file])


                # combine files 
                combined = pd.concat(files_to_combine).sort_values(['datetime', 'crawler_name'],ascending=[False,False]).reset_index(drop=True)
                combined = combined.drop_duplicates(['crawler_name', 'trust', 'datetime'])
                combined.to_csv(path_level_3, index = False)

                # make a backup and del dev files 
                move_to_backup(self, files_to_del)

            elif update_type == "today" and len(path_files) > 0:
                print(f"Warning: today processing not implemented yet in {self.name}")
            elif update_type == "past" and len(path_files) > 0:
                print(f"Warning: past processing not implemented yet in {self.name}")
    