"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
import requests
import pandas as pd
import numpy as np
import pytz 
import datetime
import itertools

from scripts.helper_functions.read_write import write_dict_2_json, read_json_2_dict, del_processed_files
from scripts.module.web_crawler.crawler import web_crawler
from scripts.module.web_driver_chrome import web_driver_chrome

class crawler_JHU_API(web_crawler):
    
    def __init__(self, date, name, trust, config, web_site, level, data = None, log = None, path_dev = {}):
        super().__init__(date, name, trust, config, web_site, level, data = None, log = None, path_dev = {})

    def get_data(self,crawler_range):
        """
        input: range -> defines which range we want to crawl
        gather data from JHU API
        """

        if crawler_range == "past": 
            r = requests.get("https://covidapi.info/api/v1/global/timeseries/2019-12-10/" + self.current_time()) 
            dict_data = r.json() 
            self.data = dict_data
            write_dict_2_json(self.get_path_tmp(crawler_range),dict_data) 

        elif crawler_range == "today":
            r = requests.get("https://covidapi.info/api/v1/global/latest") 
            dict_data = r.json() 
            self.data = dict_data
            write_dict_2_json(self.get_path_tmp(crawler_range),dict_data) 

        else:
            print(f"Warning: range is not defined in crawler_JHU_API -> {crawler_range}")

        return self
    
    def process_dev_files(self):
        """
        process data and add it to the collection 
        """

        # files to delte after processing
        files_to_del = [] 
        # files that have to be merged 
        files_to_combine = []
        # check if there exists a level_1 collections csv
        path_level_1 = os.path.join(self.config['path_root'],"data","collect",self.level + ".csv")

        if os.path.exists(path_level_1):
            df_old = pd.read_csv(path_level_1)

            # check if col exists 
            if "cumulative_recovered" not in df_old.columns:
                df_old = df_old.assign(recovered = np.nan)
                df_old = df_old.assign(cumulative_recovered = np.nan) 

            files_to_combine = [df_old]

        for update_type, path_files in self.path_dev.items():
            
            # get translation table for alpha2 to alpha3 geocode 
            path_translaion = os.path.join(self.config['path_root'],"config","translation_table","alpha2_to_alpha3.csv") 
            df_translation  = pd.read_csv(path_translaion, sep=";")

            if update_type == "today" and len(path_files) > 0:
                # every file holds a row of the level_1 csv 
                list_rows = []

                for path_file in path_files:
                    dict_file = read_json_2_dict(path_file)
                    files_to_del.append(path_file)

                    # build utc time 
                    local    = pytz.timezone("Europe/Vienna")
                    naive    = datetime.datetime.strptime(dict_file['date'], "%Y-%m-%d")
                    local_dt = local.localize(naive, is_dst=None)
                    utc_dt   = local_dt.astimezone(pytz.utc).timestamp() * 1000

                    for country_data in dict_file['result']:
                        alpha_3 = list(country_data.keys())[0]
                        # get alpha 2 
                        alpha_2 = df_translation['Alpha_3'] == alpha_3

                        data = country_data[alpha_3]
                        try:
                            alpha_2_name = df_translation[alpha_2]["Alpha_2"].values[0]
                        except:
                            print(f"Error: cant match alpha2 to alpha3 ->  alpha2 == {alpha_3}")
                            alpha_2_name = alpha_3

                        # crawler_name,trust,day,country,region,deaths,cumulative_deaths,confirmed,cumulative_confirmed,recovered,cumulative_recovered
                        row = [
                            self.name, self.trust, utc_dt, alpha_2_name, "", 
                            np.nan, data['deaths'],
                            np.nan, data['confirmed'], 
                            np.nan, data['recovered']
                        ]
                        list_rows.append(row)

                # all today data 
                df_today = pd.DataFrame(list_rows,columns=self.config['column']['level_1'])  
                files_to_combine.append(df_today)

                # combine files 
                combined = pd.concat(files_to_combine).sort_values(["day","confirmed","deaths", "recovered"],ascending=[False,False,False,False]).reset_index(drop=True)
                combined = combined.drop_duplicates(["crawler_name","day","country","recovered"])
                    
                combined.to_csv(path_level_1, index = False)
                print(f"Note: Update -> \"{path_level_1}\"")

                del_processed_files(files_to_del)


            elif update_type == "past" and len(path_files) > 0:
                for path_file in path_files:
                    dict_file = read_json_2_dict(path_file)

                    files_to_del.append(path_file)

                    list_rows = []

                    for alpha_3, country_data in dict_file["result"].items(): 
                        alpha_2 = df_translation['Alpha_3'] == alpha_3
                        try:
                            alpha_2_name = df_translation[alpha_2]["Alpha_2"].values[0]
                        except:
                            print(f"Error: cant match alpha2 to alpha3 ->  alpha2 == {alpha_3}")
                            alpha_2_name = alpha_3

                        for data in country_data:
                            
                            local = pytz.timezone ("Europe/Vienna")
                            naive = datetime.datetime.strptime(data['date'], "%Y-%m-%d")
                            local_dt = local.localize(naive, is_dst=None)
                            utc_dt = local_dt.astimezone(pytz.utc).timestamp() * 1000

                            # crawler_name,trust,day,country,region,deaths,cumulative_deaths,confirmed,cumulative_confirmed,recovered,cumulative_recovered
                            row = [
                                self.name, self.trust, utc_dt, alpha_2_name, "", 
                                np.nan, data['deaths'],
                                np.nan, data['confirmed'], 
                                np.nan, data['recovered']
                            ]
                            list_rows.append(row)
                
                # all past data 
                df_past = pd.DataFrame(list_rows,columns=self.config['column']['level_1'])  
                files_to_combine.append(df_past)

                # combine files 
                combined = pd.concat(files_to_combine).sort_values(["day","confirmed","deaths", "recovered"],ascending=[False,False,False,False]).reset_index(drop=True)
                combined = combined.drop_duplicates(["crawler_name","day","country","recovered"])
                    
                combined.to_csv(path_level_1, index = False)
                print(f"Note: Update -> \"{path_level_1}\"")

                del_processed_files(files_to_del)







