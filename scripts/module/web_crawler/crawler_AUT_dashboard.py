"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
import requests
import re
import pandas as pd 
import numpy as np
import pytz 
import datetime

from scripts.helper_functions.read_write import write_dict_2_json, read_json_2_dict, del_processed_files, move_to_backup
from scripts.module.web_crawler.crawler import web_crawler
from scripts.module.web_driver_chrome import web_driver_chrome

class crawler_AUT_dashboard(web_crawler):
    
    def __init__(self, date, name, trust, config, web_site, level, data = None, log = None, path_dev = {}):
        super().__init__(date, name, trust, config, web_site, level, data = None, log = None, path_dev = {})

    def get_data(self,crawler_range):
        """
        input: range -> defines which range we want to crawl
        gather data from gesundheitsministerium dashboard 
        """

        def get_hospitalization(chrome, link):
            chrome.driver.get(link)
            table = [i.text for i in chrome.driver.find_elements_by_css_selector("td , th")]

            # cut header 
            table = table[3:]

            dict_hospitalization = {}

            for i in range(0,int(len(table)/3)):
                index_state             = i * 3 
                index_hospitalization   = i * 3 + 1 
                index_intensive         = i * 3 + 2 

                dict_hospitalization[table[index_state]] = {
                    "hospitalization" : table[index_hospitalization],
                    "intensive" : table[index_intensive]
                }

            tests = re.findall(re.compile(r".*:.*?(\d+.*)"),chrome.driver.find_element_by_css_selector("#content p").text)[0]

            return dict_hospitalization , tests 

        def execute_js_obj(chrome, link):
            chrome.driver.get(link)
            # catch only javascript var
            re_pattern = re.compile(r".*var.*=(.*?);")
            js_txt = chrome.driver.find_element_by_xpath("/html/body/pre").text
            match = re.findall(re_pattern,js_txt)

            js_obj = chrome.driver.execute_script("return " + match[0])

            return js_obj

        if crawler_range == "today": 
            
            path_web_driver = os.path.join(self.config["path_root"],"browser","chrome","chromedriver")
            # chrome          = web_driver_chrome(path_web_driver,False)
            chrome          = web_driver_chrome(path_web_driver,headless=True)
            chrome          = chrome.init_browser()

            # go to page 
            chrome.driver.get(self.web_site)
            
            last_refresh   = chrome.driver.find_element_by_xpath("//*[@id=\"divLetzteAktualisierung\"]").text
            sum_pos_tested = chrome.driver.find_element_by_xpath("//*[@id=\"divErkrankungen\"]").text
            
            
            # table that holds data of -> hospitalization, intensive care unit and tests 
            link_to_table  = chrome.driver.find_element_by_css_selector(".text-light").get_attribute("href")
            table_hospitalization, tests   = get_hospitalization(chrome,link_to_table)

            # get the js obj 
            gender_dist     = execute_js_obj(chrome, "https://info.gesundheitsministerium.at/data/Geschlechtsverteilung.js")
            age_dist        = execute_js_obj(chrome, "https://info.gesundheitsministerium.at/data/Altersverteilung.js")
            state_dist      = execute_js_obj(chrome, "https://info.gesundheitsministerium.at/data/Bundesland.js")
            national_trend  = execute_js_obj(chrome, "https://info.gesundheitsministerium.at/data/Trend.js")
            districts       = execute_js_obj(chrome, "https://info.gesundheitsministerium.at/data/Bezirke.js")

            self.data = {
                "last_refresh" : last_refresh,
                "sum_pos_tested" : sum_pos_tested,
                "table_hospitalization" : table_hospitalization,
                "tests" : tests,
                "gender_dist" : gender_dist,
                "age_dist" : age_dist,
                "state_dist" : state_dist,
                "national_trend" : national_trend,
                "districts" : districts
                }

            write_dict_2_json(self.get_path_tmp(crawler_range),self.data) 

            chrome.driver.close()

        elif crawler_range == "past":
            print("past that crawling makes no scense at https://info.gesundheitsministerium.at")
            raise Exception

        else:
            print(f"Warning: range is not defined in crawler_JHU_API -> {crawler_range}")

        return self
    
    def process_dev_files(self): 
        """
        process data and add it to the collection 
        """

        def row_age_dist(dict_age_dist):
            """
            input: dict_age_dist -> dictionary 
            output: 
                - list_age_dist -> list 
                - list_age_dist_col -> lsit 
            convert age_dist dictionary into a list (row) and make a list of corresponding col names
            """

            list_age_dist_col = ["age_dist_" + dict_n["label"] for dict_n in dict_age_dist]

            try:
                list_age_dist = [
                    dict_age_dist[0]['y'],
                    dict_age_dist[1]['y'],
                    dict_age_dist[2]['y'],
                    dict_age_dist[3]['y'],
                    dict_age_dist[4]['y'],
                    dict_age_dist[5]['y'],
                    dict_age_dist[6]['y'],
                    dict_age_dist[7]['y']
                    ]
            except Exception as error:
                print(f"Error: AUT_dashboard in row_age_dist")
                print(error)
                list_age_dist = [
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan,
                    np.nan
                    ]

            return list_age_dist, list_age_dist_col

        def row_crawler(self,date_human):
            """
            crawler informations subset 
            """

            local = pytz.timezone ("Europe/Vienna")
            naive = datetime.datetime.strptime(date_human, "%d.%m.%Y %H:%M.%S")
            local_dt = local.localize(naive, is_dst=None)
            utc_dt = local_dt.astimezone(pytz.utc).timestamp() * 1000

            crawler = [self.name,self.trust, utc_dt]
            crawler_col = ["crawler_name","trust", "datetime"]

            return crawler, crawler_col

        def row_tests(dict_file):

            list_tests = [int(dict_file['tests'].replace(".","")), int(dict_file['sum_pos_tested'])]
            tests_col = ["tests_sum", "tests_sum_pos"]

            return list_tests,tests_col

        def row_district(dict_districts):
            
            list_district = []
            list_district_col = []

            # and correct vowels ... 
            for district in dict_districts:

                district_name = district["label"].replace(" ","_")
                
                if district_name == "Bruck-MÃ¼rzzuschlag":
                    district_name = "Bruck-Mürzzuschlag"

                elif district_name == "GÃ¤nserndorf":
                    district_name = "Gänserndorf"

                elif district_name == "GmÃ¼nd":
                    district_name = "Gmünd"

                elif district_name == "GÃ¼ssing":
                    district_name = "Güssing"

                elif district_name == "Hartberg-Fürstenfeld":
                    district_name = "Gänserndorf"

                elif district_name == "KitzbÃ¼hel":
                    district_name = "Kitzbühel"

                elif district_name == "MÃ¶dling":
                    district_name = "Mödling"

                elif district_name == "Sankt_PÃ¶lten(Land)":
                    district_name = "Sankt_Pölten(Land)"

                elif district_name == "Sankt_PÃ¶lten(Stadt)":
                    district_name = "Sankt_Pölten(Stadt)"

                elif district_name == "SchÃ¤rding":
                    district_name = "Schärding"

                elif district_name == "SÃ¼doststeiermark":
                    district_name = "Südoststeiermark"

                elif district_name == "VÃ¶cklabruck":
                    district_name = "Vöcklabruck"

                elif district_name == "VÃ¶lkermarkt":
                    district_name = "Völkermarkt"

                elif district_name == "GrÃ¶bming":
                    district_name = "Gröbming"
                    
                list_district.append(district['y'])
                list_district_col.append("district_cases_" + district_name)

            return list_district, list_district_col

        def row_gender_dist(dict_gender_dist):
            
            list_gender_dist = [] 
            list_gender_dist_col = []

            for gender in dict_gender_dist:
                if gender["label"] == 'weiblich':
                    list_gender_dist.append(gender['y'])
                    list_gender_dist_col.append("gender_dist_weiblich")
                else:
                    list_gender_dist.append(gender['y'])
                    list_gender_dist_col.append("gender_dist_männlich")

            return list_gender_dist, list_gender_dist_col

        def row_state_dist(dict_state_dist):

            list_state_dist = [] 
            list_state_dist_col = []

            col_starter = "state_cases_"
            
            for state in dict_state_dist:
                if state["label"] == 'OÃ–':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Oberösterreich")

                elif state["label"] == 'NÃ–':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Niederösterreich")

                elif state["label"] == 'Bgld':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Burgenland")

                elif state["label"] == 'Ktn':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Kärnten")

                elif state["label"] == 'Sbg':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Salzburg")

                elif state["label"] == 'Stmk':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Steiermark")

                elif state["label"] == 'T':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Tirol")

                elif state["label"] == 'Vbg':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Vorarlberg")

                elif state["label"] == 'W':
                    list_state_dist.append(state['y'])
                    list_state_dist_col.append(col_starter+"Wien")
                
                else:
                    label = state["label"]
                    print(f"Error: cant parse state {label}")

            return list_state_dist, list_state_dist_col

        def row_hospitalization(dict_hospitalization):
            
            list_hospitalization = []
            list_hospitalization_col = []

            for state, sub_dict in dict_hospitalization.items():
                hospitalization = int(sub_dict['hospitalization'].replace(".",""))
                intensive = int(sub_dict['intensive'].replace(".",""))
                if state != "Österreich gesamt":
                    list_hospitalization     = list_hospitalization + [hospitalization, intensive]
                    list_hospitalization_col = list_hospitalization_col + ["hospitalization_"+ state, "intensive_" + state] 
                else:
                    list_hospitalization     = list_hospitalization + [hospitalization, intensive]
                    list_hospitalization_col = list_hospitalization_col + ["hospitalization_Österreich", "intensive_Österreich"] 

            return list_hospitalization, list_hospitalization_col

        def build_dataframe(row,column):
            """
            input: 
                - row -> list 
                - column -> list 
            output: 
                df -> pandas dataframe 
            build the pandas data frame
            """
            # structure of the data frame 
            dict_pd = {column[ii]:[row[ii]] for ii, i in enumerate(row,0)}

            # add nan for not known values from this website 
            for col in column[len(dict_pd):]:
                dict_pd[col] = [np.nan]

            df = pd.DataFrame.from_dict(dict_pd)
            return df 

        # files to delte after processing
        files_to_del = [] 
        # files that have to be merged 
        files_to_combine = []
        # check if there exists a level_1 collections csv
        path_level_3 = os.path.join(self.config['path_root'],"data","collect",self.level + ".csv")

        if os.path.exists(path_level_3):
            files_to_combine = [pd.read_csv(path_level_3)]

        for update_type, path_files in self.path_dev.items():
            if update_type == "today":
                for path_file in path_files:
                    dict_file = read_json_2_dict(path_file)

                    crawler, crawler_col             = row_crawler(self,dict_file['last_refresh'])
                    list_age_dist, list_age_dist_col = row_age_dist(dict_file['age_dist'])
                    list_district, list_district_col = row_district(dict_file['districts'])
                    list_tests,tests_col             = row_tests(dict_file)

                    national_trend_latest = [dict_file['national_trend'][len(dict_file['national_trend'])-1]["y"]]
                    national_trend_col     = ["national_latest_cases"]

                    list_gender_dist, list_gender_dist_col = row_gender_dist(dict_file['gender_dist'])
                    list_state_dist, list_state_dist_col   = row_state_dist(dict_file['state_dist'])
                     
                    list_hospitalization, list_hospitalization_col = row_hospitalization(dict_file['table_hospitalization'])

                    # build dataframe 
                    row = crawler + list_age_dist + list_district + list_tests + national_trend_latest + list_gender_dist + list_state_dist + list_hospitalization
                    df = build_dataframe(row,self.config['column']['level_3'])

                    files_to_combine.append(df)
                    files_to_del.append(path_file) 
                
                # combine files 
                combined = pd.concat(files_to_combine).sort_values(['datetime', 'crawler_name'],ascending=[False,False]).reset_index(drop=True)
                combined = combined.drop_duplicates(['crawler_name', 'trust', 'datetime'])
                combined.to_csv(path_level_3, index = False)

                # make a backup and del dev files 
                move_to_backup(self, files_to_del)

            elif update_type == "past" and len(path_files) > 0:
                print(f"Warning: past processing not implemented yet in {self.name}")
