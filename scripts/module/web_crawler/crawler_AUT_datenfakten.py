"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
import requests
import re
import pandas as pd 
import numpy as np
import pytz 
import datetime

from scripts.helper_functions.read_write import write_dict_2_json, read_json_2_dict, del_processed_files, move_to_backup
from scripts.module.web_crawler.crawler import web_crawler
from scripts.module.web_driver_chrome import web_driver_chrome

class crawler_AUT_datenfakten(web_crawler):
    
    def __init__(self, date, name, trust, config, web_site, level, data = None, log = None, path_dev = {}):
        super().__init__(date, name, trust, config, web_site, level, data = None, log = None, path_dev = {})

    def get_data(self,crawler_range):
        """
        input: range -> defines which range we want to crawl
        gather data from datenfakten.at  
        """

        def get_datetime(chrome):
            """
            get current update date of website and convert to utc 
            """

            date_raw = chrome.driver.find_element_by_css_selector(".page-title-box > p:nth-child(2)").text

            re_pattern = re.compile(r".*?,\s(.*)\sUhr")
            re_match   = re.findall(re_pattern, date_raw)[0]

            local = pytz.timezone("Europe/Vienna")
            naive = datetime.datetime.strptime(re_match, "%d.%m.%Y, %H:%M")
            local_dt = local.localize(naive, is_dst=None)
            utc_dt = local_dt.astimezone(pytz.utc).timestamp() * 1000

            return utc_dt 

        def get_table_state(chrome):
            """
            get all values from table states 
            """

            table_states = {
                "Bundesland" : [],
                "infiziert" : [],
                "Zunahme infiziert" : [],
                "gestorben" : [],
                "genesen" : [],
                "derzeit krank" : []
            }
            css_selector = "body > div:nth-child(1) > div > div > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div > div > div > table"
            table_raw = [i.text for i in chrome.driver.find_elements_by_css_selector(".mb-3+ .table-responsive-sm td , .mb-3+ .table-responsive-sm th")]

            # cut of header 
            table_raw = table_raw[6:]

            row_size = int(len(table_raw)/6)

            for i in list(range(0,10)):
                table_states["Bundesland"].append(table_raw[i*6])
                table_states["infiziert"].append(table_raw[i*6+1])
                table_states["Zunahme infiziert"].append(table_raw[i*6+2])
                table_states["gestorben"].append(table_raw[i*6+3])
                table_states["genesen"].append(table_raw[i*6+4])
                table_states["derzeit krank"].append(table_raw[i*6+5])

            return table_states
        
        def get_district_cases(chrome):
            """
            get table district cases 
            """

            table_raw = [i.text for i in chrome.driver.find_elements_by_css_selector(".tabelle-scrollbar td")]

            district_cases = {}

            for ii,i in enumerate(range(0,int(len(table_raw)/2)),0):
                index_district = int(i * 2)
                index_cases    = int(i * 2 + 1)

                district_cases[table_raw[index_district]] = int(table_raw[index_cases])

            return district_cases

        if crawler_range == "today": 
            
            path_web_driver = os.path.join(self.config["path_root"],"browser","chrome","chromedriver")
            # chrome          = web_driver_chrome(path_web_driver,False)
            chrome          = web_driver_chrome(path_web_driver,headless=True)
            chrome          = chrome.init_browser()

            # go to page 
            chrome.driver.get(self.web_site)
            
            utc_datetime = get_datetime(chrome)

            table_states = get_table_state(chrome)

            district_cases = get_district_cases(chrome)

            self.data = {
                "last_refresh" : utc_datetime,
                "state_table" : table_states,
                "districts" : district_cases
                }

            write_dict_2_json(self.get_path_tmp(crawler_range),self.data) 
            
            chrome.driver.close()

        elif crawler_range == "past":
            print("past that crawling makes no scense at https://coronavirus.datenfakten.at/")
            raise Exception

        else:
            print(f"Warning: range is not defined in crawler_JHU_API -> {crawler_range}")

        return self

    def process_dev_files(self): 
        """
        process data and add it to the collection 
        """ 

        def build_dataframe(dict_crawler,dict_district,dict_state,column):
            """
            input: 
                - row -> list 
                - column -> list 
            output: 
                df -> pandas dataframe 
            build the pandas data frame
            """

            # build df struct 
            dict_pd = {i:[np.nan] for i in column}

            for k,v in dict_pd.items():
                if k in dict_crawler.keys():
                    dict_pd[k] = [dict_crawler[k]]
                elif k in dict_district.keys():
                    dict_pd[k] = [dict_district[k]]
                elif k in dict_state.keys():
                    dict_pd[k] = [dict_state[k]]
                else:
                    pass
                    # print(f"Error: cant match key -> {k}")

            df = pd.DataFrame.from_dict(dict_pd)
            return df 
        
        def get_row_crawler(crawler,dict_file):
            """
            crawler data for csv 
            """

            dict_crawler = {
                "crawler_name" : crawler.name,
                "trust"        : crawler.trust,
                "datetime"     : dict_file['last_refresh']
            }

            return dict_crawler

        def get_row_district(dict_districts):
            
            dict_district = {"district_cases_" + k.replace(" ","_"):v for k,v in dict_districts.items()}

            return dict_district

        def get_state_info(state_table):
            
            dict_state_info = {}

            df_states = pd.DataFrame.from_dict(state_table)

            for index, row in df_states.iterrows():

                if row["Bundesland"] == "GESAMT":
                    
                    key = "national_latest_cases"
                    value = row["infiziert"]

                    dict_state_info[key] = int(value)

                else:
                    key_cases   = "state_cases_" + row["Bundesland"]
                    value_cases = float(row["infiziert"])
                    dict_state_info[key_cases] = value_cases

                    key_deaths   = "state_deaths_" + row["Bundesland"]
                    value_deaths = float(row["gestorben"])
                    dict_state_info[key_deaths] = value_deaths

                    key_recovered   = "state_recovered_" + row["Bundesland"]
                    value_recovered = float(row["genesen"])
                    dict_state_info[key_recovered] = value_recovered

                    key_ill   = "state_ill_" + row["Bundesland"]
                    value_ill = float(row["derzeit krank"])
                    dict_state_info[key_ill] = value_ill

            return dict_state_info

        # files to delte after processing
        files_to_del = [] 
        # files that have to be merged 
        files_to_combine = []
        # check if there exists a level_1 collections csv
        path_level_3 = os.path.join(self.config['path_root'],"data","collect",self.level + ".csv")

        if os.path.exists(path_level_3):
            files_to_combine = [pd.read_csv(path_level_3)]

        for update_type, path_files in self.path_dev.items():
            if update_type == "today":
                for path_file in path_files:
                    dict_file = read_json_2_dict(path_file)

                    dict_crawler    = get_row_crawler(self,dict_file)
                    dict_district   = get_row_district(dict_file['districts'])
                    dict_state      = get_state_info(dict_file["state_table"])


                    df = build_dataframe(dict_crawler,dict_district,dict_state,self.config['column']['level_3'])
                    
                    files_to_combine.append(df)
                    files_to_del.append(path_file) 
                
                # combine files 
                combined = pd.concat(files_to_combine).sort_values(['datetime', 'crawler_name'],ascending=[False,False]).reset_index(drop=True)
                combined = combined.drop_duplicates(['crawler_name', 'trust', 'datetime'])
                combined.to_csv(path_level_3, index = False)

                # make a backup and del dev files 
                move_to_backup(self, files_to_del)

            elif update_type == "past" and len(path_files) > 0:
                print(f"Warning: past processing not implemented yet in {self.name}")
