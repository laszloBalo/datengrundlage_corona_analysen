"""
FH JOANNEUM
László Baló
laszlo.balo@edu.fh-joanneum
"""

import os 
import lxml
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.select import Select
from bs4 import BeautifulSoup

class web_driver_chrome():

    def __init__(self, path, headless, driver = None, soup_lxml = None):
        self.path       = path 
        self.headless   = headless
        self.driver     = driver 
        self.soup_lxml  = soup_lxml

    def init_browser(self): 
        """
        input: path to the browser executable
        output: browser driver object 
        init the broser
        """

        options = Options()
        if self.headless:
            options.add_argument('--headless')

        options.add_experimental_option("prefs", {
        "download.default_directory": os.path.join(os.path.abspath("."),"data","dev"),
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing.enabled": True,
        "safebrowsing_for_trusted_sources_enabled": False,
        })
        

        self.driver = webdriver.Chrome(self.path,chrome_options=options)

        return self

    def click_button(self, css_selector):
        """
        input: driver and css_selector string 
        ouput: None 
        click a given button defined by an xpath string
        """
        
        python_button = self.driver.find_element_by_css_selector(css_selector)
        python_button.click()

    def get_source_lxml(self):
        """
        input: driver 
        output: soup object that holds lxml of the sourced page 
        """
        
        html = self.driver.page_source
        self.soup_lxml = BeautifulSoup(html, "lxml")

        return self 
