# Datengrundlage_Corona_Analysen
# Introduction
The goal of this project is to get insight about data regarding COVID-19. As a data scientist we want to do our own analytics; thus, we need our own data that we trust. 
 The goal of this project is to: 
-	Crawl data from the internet and compare it to each other 
-	Simple data analysis in R
-	We want to divide the data into official and unofficial sets (proof that data is correct)
- We want to divide the data into differrent levels of detail
-	The main focus lies here on Austria; thus, Austria is dealt with in a more detailed way 

### Process
![process.png](./doc/process.png)

# Requirements
- check out the python requirement file in the root space for all dependencies (Python 3.8.0)
- you may start with executing the main_crawler.py to get the folder struct going 
- there must be a config (if you clone you will have a json included)
- Selenium web driver is not included 
  - place it under the folder "./browser/\[browser_type\]\\\[browser_name\]"
  - currently setup is made for chrome
- the config file holds the update cycle ->  "settings"

# Local Setup

### Clone the repo locally
```
git clone https://gitlab.com/laszloBalo/datengrundlage_corona_analysen.git
cd datengrundlage_corona_analysen
```
### Setup a new Python Environment and source it
Python version 3.8+ would be required to run the project (MacOS)
```
python3.8 -m venv venv
source venv/bin/activate
```
### Setup Python packages 
```
pip install -r python_req.txt
```

### Bugs 
- cant match specific geocodes alpha3 -> alpha2 
  - WBG, RKS, MSZ, DPS
- sometimes WHO_dashboards takes a long time until a request is recieved 
- AUT Dashboard 14.04.2020 
  -  https://info.gesundheitsministerium.at/ ... was completely changed, so the AUT_dashboard crawler is useless 
- AUT gesundheits ... 
  - some district names where changed or joined since dashboard update 
  - when downloading zip file it could be unauthorized -> the file is not found 
  - age_dist -> due to new cols 

### Nice to have features 
- geocode translation table -> we would not require to write NA 
- calculate cumulative death/confirmed for crawlers where col is NA 
- better col names for AUT_dashboard ->  currently origin names from website are used ... -> translation table
  - problems with mutated vowel -> currently bad solution 
- log files 