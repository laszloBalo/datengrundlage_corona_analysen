# Notes 

## useful websites 
- https://www.worldometers.info/coronavirus/
    - we want to prove our gathered data with this website 
- https://www.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6
  - we want to prove our gathered data with this website
- https://covidapi.info 
  - API for JHU
- https://data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data/resource/260bbbde-2316-40eb-aec3-7cd7bfc2f590
  - EU open data portal

### Austria 
- cant find detailed time series of Austria ... 
- only dashboard found that has data over time 
- other websites are updated daily but there only hold data about one day 
- https://info.gesundheitsministerium.at
  - (official)
- http://dashcoch-at.herokuapp.com/ 
  - claims that information is from official websites
- https://www.sozialministerium.at/Informationen-zum-Coronavirus/Dashboard/Zahlen-zur-Hospitalisierung
  - Hospitalisierung und Aufnahmen in Intensivstation

### AUT_dashboard data - processing 
- We crawle the data form https://info.gesundheitsministerium.at
- Every data set except national trend is pointing to the current date 
- Thus for the level_3 csv file national trend is ommited because this information may be obtained from level_1 csv files 
  - from national trend we only take the latest value
- one crawle is formated into a single row -> one row is one instance of a crawler 
- due to this one row gets very messy but it may be seperated again in some post-processing/analysis tasks later 
- a translation table is needed for the states 

#### very messy would need a cleanup ... 


## Basic Idea 

Take all information that you get and cross-validate these 

## User story 
  - I want to get a world wide information of the corona virus (Level_1)
  - I want to get deeper insights of countries sorounding Austria (Level_2) 
  - I want to get the all informations regarding the cov-19 depending Austria (Level_3)

## Dev story 
  - I want to get websites and folder struct etc. from a config json 
    - folder struct 
    - web sites with level struct for different functions 
    - crawler settings 

## process data 
- get all data gathered in the dev folder and structure it into the three levels of detail defined in this project 
- delete obvious duplicates 

## R 
- fast testing of output results 
  - some plots over time 
