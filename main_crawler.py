"""
FH JOANNEUM 
László Baló 
laszlo.balo@edu.fh-joanneum
"""

import os 
from scripts.helper_functions.read_write import *
from scripts.helper_functions.web_crawler_setup import * 

if __name__ == "__main__":
    print("########        Datengrundlage_Corona_Analysen        ########\n") 

    # check for folder struct 
    path_root = os.path.abspath(".")
    config_file = check_config(path_root)
    check_folder_struct(path_root,config_file)    

    # setup all web_crawlers defined in the config files
    list_web_crawler = web_crawler_setup(path_root,config_file)

    """ crawl data in the past until now"""
    # for web_crawler in list_web_crawler:
    #     try:
    #         web_crawler.get_data("past")
    #         print(f"Note: {web_crawler.name} successful")
    #     except Exception as error:
    #         print(f"Error: crawler past -> {web_crawler.name}")
    #         print(error)

    """ crawl data updates """
    for web_crawler in list_web_crawler:
        try:
            print(f"\nNote: {web_crawler.name} start")
            web_crawler.get_data("today")
            print(f"Note: {web_crawler.name} successful\n")
        except Exception as error:
            print(f"Error: crawler today -> {web_crawler.name}")
            print(str(error) + "\n")
    
print("\n########        Datengrundlage_Corona_Analysen        ########")
    